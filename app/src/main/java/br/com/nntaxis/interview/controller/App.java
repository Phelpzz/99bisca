package br.com.nntaxis.interview.controller;

import android.app.Application;

/**
 * Created by FBisca on 02/10/2015.
 */
public class App extends Application {

    private static App instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }

    public static App getInstance() {
        if (instance == null) {
            throw new RuntimeException("Application is null");
        }
        return instance;
    }
}
