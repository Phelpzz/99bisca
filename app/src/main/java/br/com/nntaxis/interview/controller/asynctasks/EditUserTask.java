package br.com.nntaxis.interview.controller.asynctasks;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.nntaxis.interview.controller.Constants;
import br.com.nntaxis.interview.controller.asynctasks.generic.AbstractTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.network.Network;
import br.com.nntaxis.interview.model.User;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class EditUserTask extends AbstractTask<EditUserTask.Param, Void, EditUserTask.Result> {

    @Override
    protected Result processInBackground(Param[] params) throws RuntimeException, JSONException {
        Network.Response response = Network.create(Constants.HOST + Constants.USER_SERVICE + "/" + params[0].user.getId())
                .setMethod(Network.Method.POST)
                .addParameter("name", params[0].user.getName())
                .request();

        if (response.getError() != null) {
            throw new TaskException(response.getError());
        }

        if (response.getResponseCode() == 200) {

            JSONObject object = new JSONObject(response.getResponse());
            params[0].user.setId(object.getInt("id"));
            params[0].user.setName(object.getString("name"));

            return new Result(params[0].user);
        }

        throw new ServiceException("", response.getResponseCode());
    }

    public static class Param {
        private User user;

        public Param(User user) {
            this.user = user;
        }
    }

    public class Result {
        private User user;

        public Result(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }
}
