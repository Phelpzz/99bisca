package br.com.nntaxis.interview.controller;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class Constants {

    public static final String HOST = "http://ec2-54-175-42-14.compute-1.amazonaws.com:8080",
                USER_SERVICE = "/v1/users",
                RIDE_SERVICE = "/v1/ride";
}
