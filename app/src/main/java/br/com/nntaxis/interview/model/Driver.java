package br.com.nntaxis.interview.model;

import android.os.Parcel;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import br.com.nntaxis.interview.model.generic.GenericEntity;

/**
 * Created by FBisca on 02/10/2015.
 */
public class Driver extends GenericEntity {

    private Integer id;
    private LatLng latLng;
    private boolean available;

    public Driver() {
    }

    protected Driver(Parcel in) {
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.latLng = in.readParcelable(LatLng.class.getClassLoader());
        this.available = in.readByte() != 0;
    }

    public Driver(JSONObject jsonObject) {
        this.setAvailable(jsonObject.optBoolean("driverAvailable"));
        this.setId(jsonObject.optInt("driverId"));

        LatLng latLng = new LatLng(jsonObject.optDouble("latitude"), jsonObject.optDouble("longitude"));
        this.setLatLng(latLng);
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Driver driver = (Driver) o;

        return !(id != null ? !id.equals(driver.id) : driver.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeParcelable(this.latLng, 0);
        dest.writeByte(available ? (byte) 1 : (byte) 0);
    }


    public static final Creator<Driver> CREATOR = new Creator<Driver>() {
        public Driver createFromParcel(Parcel source) {
            return new Driver(source);
        }

        public Driver[] newArray(int size) {
            return new Driver[size];
        }
    };
}
