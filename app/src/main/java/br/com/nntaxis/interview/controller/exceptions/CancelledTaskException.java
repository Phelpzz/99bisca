package br.com.nntaxis.interview.controller.exceptions;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class CancelledTaskException extends TaskException {

    public CancelledTaskException() {
        super();
    }
}
