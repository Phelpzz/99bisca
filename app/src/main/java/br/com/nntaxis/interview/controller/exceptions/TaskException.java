package br.com.nntaxis.interview.controller.exceptions;

/**
 * Created by FBisca on 12/11/2015.
 */
public class TaskException extends RuntimeException {

    public TaskException() {
        super();
    }

    public TaskException(Throwable throwable) {
        super(throwable);
    }

}
