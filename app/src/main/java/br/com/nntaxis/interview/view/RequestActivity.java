package br.com.nntaxis.interview.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.nntaxis.interview.R;
import br.com.nntaxis.interview.controller.asynctasks.RequestDriverTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.interfaces.TaskListener;
import br.com.nntaxis.interview.view.generic.GenericActivity;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class RequestActivity extends GenericActivity implements TaskListener, View.OnClickListener {

    public static final String ARG_MAP = "arg_map", ARG_LOCATION = "arg_location";

    private ImageView mImgMap;
    private TextView mTxtStatus;
    private ProgressBar mProgressBar;
    private Button mBtnTryAgain;
    private ImageView mImgSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_request);
        loadViews();

        executeTask();
    }

    private void loadViews() {
        mImgMap = (ImageView) findViewById(R.id.img_map);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mTxtStatus = (TextView) findViewById(R.id.txt_status);
        mBtnTryAgain = (Button) findViewById(R.id.btn_try_again);
        mImgSuccess = (ImageView) findViewById(R.id.img_success);
        mBtnTryAgain.setOnClickListener(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        byte[] mapBitmap = getIntent().getByteArrayExtra(ARG_MAP);
        if (mapBitmap != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(mapBitmap, 0, mapBitmap.length);
            mImgMap.setImageBitmap(bitmap);
        } else {
            mImgMap.setVisibility(View.GONE);
        }
    }

    private void executeTask() {
        RequestDriverTask task = new RequestDriverTask();
        task.setListener(this);
        task.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskStart(Class<?> taskClass) {
        if (taskClass.equals(RequestDriverTask.class)) {
            mTxtStatus.setText(R.string.lbl_looking_for_drivers);
            mProgressBar.setVisibility(View.VISIBLE);
            mBtnTryAgain.setVisibility(View.GONE);
        }
    }

    @Override
    public void onTaskEnd(Class<?> taskClass, Object data, TaskException error) {
        if (RequestDriverTask.class.equals(taskClass)) {
            mProgressBar.setVisibility(View.GONE);
            if (error == null) {
                String result = (String) data;
                mTxtStatus.setText(result);
                mImgSuccess.setVisibility(View.VISIBLE);
            } else if (error instanceof ServiceException) {
                mTxtStatus.setText(error.getMessage());
                mBtnTryAgain.setVisibility(View.VISIBLE);
            } else {
                mTxtStatus.setText(R.string.lbl_request_error);
                mBtnTryAgain.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.equals(mBtnTryAgain)) {
            executeTask();
        }
    }
}
