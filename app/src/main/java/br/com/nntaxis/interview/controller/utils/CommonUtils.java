package br.com.nntaxis.interview.controller.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by FBisca on 02/10/2015.
 */
public class CommonUtils {

    public static LatLng createLatLng(Location location) {
        if (location == null) {
            return new LatLng(0d, 0d);
        }
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static String formatLatLng(LatLng latLng) {
        if (latLng != null) {
            return String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude);
        }
        return "";
    }
}
