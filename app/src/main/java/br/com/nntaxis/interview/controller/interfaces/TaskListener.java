package br.com.nntaxis.interview.controller.interfaces;


import br.com.nntaxis.interview.controller.exceptions.TaskException;

/**
 * Created by FBisca on 12/11/2015.
 */
public interface TaskListener {

    void onTaskStart(Class<?> taskClass);
    void onTaskEnd(Class<?> taskClass, Object data, TaskException error);

}
