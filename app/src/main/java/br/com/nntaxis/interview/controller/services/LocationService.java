package br.com.nntaxis.interview.controller.services;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

/**
 * Created by FBisca on 02/10/2015.
 */
public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private ArrayList<SimpleLocationListener> mLocationListeners;
    private LocationBinder mBinder = new LocationBinder();
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;

    @Override
    public void onCreate() {
        super.onCreate();
        mLocationListeners = new ArrayList<>();
        buildGoogleApiClient();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        clearListeners();
        mGoogleApiClient.disconnect();
    }

    private void clearListeners() {
        mLocationListeners.clear();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopLocationUpdates();
        return super.onUnbind(intent);
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        startLocationUpdates();
    }

    private void startLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, createRequest(), this);
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private LocationRequest createRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(30000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.isSuccess()) {
            dispatchOnConnectionFailed(connectionResult);
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLocation = location;
        dispatchOnLocationReceived(location);
    }

    private void dispatchOnLocationReceived(Location location) {
        for (SimpleLocationListener listener : mLocationListeners) {
            listener.onLocationReceived(location);
        }
    }

    private void dispatchOnConnectionFailed(ConnectionResult result) {
        for (SimpleLocationListener listener : mLocationListeners) {
            listener.onConnectionFailed(result);
        }
    }

    public class LocationBinder extends Binder {

        public void addListener(SimpleLocationListener listener) {
            if (!mLocationListeners.contains(listener)) {
                mLocationListeners.add(listener);
                if (mLocation != null) {
                    listener.onLocationReceived(mLocation);
                }
            }
        }

        public void removeListener(SimpleLocationListener listener) {
            mLocationListeners.remove(listener);
        }

        public LocationService getService() {
            return LocationService.this;
        }

    }

    public interface SimpleLocationListener {
        void onLocationReceived(Location location);
        void onConnectionFailed(ConnectionResult result);
    }

}
