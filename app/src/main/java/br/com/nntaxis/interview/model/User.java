package br.com.nntaxis.interview.model;

import android.os.Parcel;

import br.com.nntaxis.interview.model.generic.GenericEntity;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class User extends GenericEntity {

    private int id;
    private String name;
    private String locationUrl;

    public User() {
        super();
    }

    protected User(Parcel in) {
        super();
        this.id = in.readInt();
        this.name = in.readString();
        this.locationUrl = in.readString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocationUrl() {
        return locationUrl;
    }

    public void setLocationUrl(String locationUrl) {
        this.locationUrl = locationUrl;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.locationUrl);
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
