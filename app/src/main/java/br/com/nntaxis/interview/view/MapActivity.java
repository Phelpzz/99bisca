package br.com.nntaxis.interview.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Typeface;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import br.com.nntaxis.interview.R;
import br.com.nntaxis.interview.controller.SharedPref;
import br.com.nntaxis.interview.controller.asynctasks.LastLocationTask;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.interfaces.TaskListener;
import br.com.nntaxis.interview.controller.services.LocationService;
import br.com.nntaxis.interview.controller.utils.CommonUtils;
import br.com.nntaxis.interview.model.Driver;
import br.com.nntaxis.interview.model.User;
import br.com.nntaxis.interview.view.generic.GenericActivity;
import br.com.nntaxis.interview.view.spannable.CustomSpan;

/**
 * Created by FBisca on 02/10/2015.
 */
public class MapActivity extends GenericActivity implements OnMapReadyCallback, ServiceConnection, LocationService.SimpleLocationListener, GoogleMap.OnMarkerClickListener, View.OnClickListener, TaskListener, GoogleMap.OnMarkerDragListener {

    public static final int REQUEST_CODE_RESOLUTION = 0x23;
    private static final float DEFAULT_ZOOM = 16f;

    private ArrayList<Driver> mDriverList = new ArrayList<>();
    private ConcurrentHashMap<Driver, Marker> mMarkers = new ConcurrentHashMap<>();
    private Marker mMyMarker;
    private LatLng mSavedMarkerLocation;

    private Handler mHandler = new Handler();

    private View mLoading;
    private Button mBtnRefresh, mBtnRequestPositive, mBtnRequestNegative;
    private ImageButton mBtnMyLocation, mBtnCallTaxi;
    private View mCardRequest;
    private TextView mTxtRequest;

    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private LatLng mLastCameraLocation;

    private LastLocationTask mTask;
    private LocationService mLocationService;
    private LocationService.LocationBinder mBinder;
    private Location mLastLocation;

    private boolean mMapSetup = false;
    private boolean mRunningAnim = false;
    private boolean mUpdatingLocationMarker = true;

    private long mUpdateInterval = 10000l;

    private Runnable mTaskHandler = new Runnable() {
        @Override
        public void run() {
            if (mGoogleMap != null) {
                executeTask();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindService(new Intent(this, LocationService.class), this, Context.BIND_AUTO_CREATE);

        setContentView(R.layout.layout_map);
        loadViews();

        if (savedInstanceState == null) {
            openMapFragment();
        } else {
            mDriverList = savedInstanceState.getParcelableArrayList("drivers");
            mMapSetup = savedInstanceState.getBoolean("mapSetup");
            mSavedMarkerLocation = savedInstanceState.getParcelable("markerLocation");

            mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_container);
            if (mMapFragment != null) {
                mMapFragment.getMapAsync(this);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserInfo();
    }

    private void updateUserInfo() {
        User user = SharedPref.getUser();
        if (user == null) {
            mTxtRequest.setText(R.string.lbl_request_driver);
        } else {
            String preText = getString(R.string.lbl_request_driver_hello) + " ";
            String text = getString(R.string.lbl_request_driver_logged, user.getName());

            SpannableStringBuilder span = new SpannableStringBuilder(preText + text);
            span.setSpan(new CustomSpan(Typeface.create("sans-serif-medium", Typeface.NORMAL),
                    R.color.black_100), preText.length(), preText.length() + user.getName().length(),
                    Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            mTxtRequest.setText(span);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(this);

        if (mBinder != null) {
            mBinder.removeListener(this);
        }

        if (mTask != null) {
            mTask.cancel(true);
        }

        if (mHandler != null) {
            mHandler.removeCallbacks(mTaskHandler);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void loadViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mLoading = findViewById(R.id.loading);
        mBtnRefresh = (Button) findViewById(R.id.btn_refresh);
        mBtnCallTaxi = (ImageButton) findViewById(R.id.btn_call_driver);
        mBtnMyLocation = (ImageButton) findViewById(R.id.btn_my_location);
        mCardRequest = findViewById(R.id.card_driver_request);
        mBtnRequestPositive = (Button) findViewById(R.id.btn_request_positive);
        mBtnRequestNegative = (Button) findViewById(R.id.btn_request_negative);
        mTxtRequest = (TextView) findViewById(R.id.txt_driver_request);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_USE_LOGO);
            toolbar.setLogo(R.mipmap.ic_launcher);
        }

        mBtnRefresh.setOnClickListener(this);
        mBtnMyLocation.setOnClickListener(this);
        mBtnCallTaxi.setOnClickListener(this);
        mBtnRequestNegative.setOnClickListener(this);
        mBtnRequestPositive.setOnClickListener(this);

    }

    private void openMapFragment() {
        mMapFragment = createMapFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.map_container, mMapFragment).commit();
    }

    private SupportMapFragment createMapFragment() {
        GoogleMapOptions options = new GoogleMapOptions()
                .rotateGesturesEnabled(false)
                .tiltGesturesEnabled(false)
                .mapType(GoogleMap.MAP_TYPE_NORMAL)
                .zoomControlsEnabled(true)
                .scrollGesturesEnabled(true);


        mMapFragment = SupportMapFragment.newInstance(options);
        mMapFragment.getMapAsync(this);

        return mMapFragment;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMyLocationEnabled(false);
        mGoogleMap.setOnMarkerDragListener(this);

        if (mSavedMarkerLocation != null) {
            addMyMarker(mSavedMarkerLocation);
            mUpdatingLocationMarker = false;
            mSavedMarkerLocation = null;
        }

        mTaskHandler.run();
        updateUI();


    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBinder = (LocationService.LocationBinder) service;
        mLocationService = mBinder.getService();
        mBinder.addListener(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {

    }

    @Override
    public void onLocationReceived(Location location) {
        this.mLastLocation = location;
        if (mGoogleMap != null) {
            if (!mMapSetup) {
                mMapSetup = true;
                CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(
                        CameraPosition.fromLatLngZoom(CommonUtils.createLatLng(location), DEFAULT_ZOOM));
                mGoogleMap.moveCamera(cameraUpdate);
                mGoogleMap.setOnMarkerClickListener(this);

                forceExecute();
            }

            if (mUpdatingLocationMarker) {
                LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
                addMyMarker(position);
            }
        }
    }

    private void addMyMarker(LatLng position) {
        if (mMyMarker != null) {
            animateMarkerPosition(position, mMyMarker);
        } else {
            mMyMarker = mGoogleMap.addMarker(new MarkerOptions().position(position).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_my_marker)).draggable(true));
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (result.hasResolution()) {
            try {
                result.startResolutionForResult(this, REQUEST_CODE_RESOLUTION);
            } catch (IntentSender.SendIntentException e) {

            }
        }
    }


    private void updateUI() {
        if (mGoogleMap != null) {
            for (Driver driver : mDriverList) {
                if (mMarkers.get(driver) != null) {
                    animateMarkerPosition(driver.getLatLng(), mMarkers.get(driver));
                } else {
                    addMarker(driver);
                }
            }
            removeOldMarks();

        }
    }

    private void addMarker(Driver driver) {
        if (mGoogleMap != null) {
            Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(driver.getLatLng()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pin)));

            mMarkers.put(driver, marker);
        }
    }

    private void animateMarkerPosition(final LatLng newLocation, final Marker marker) {
        if (mGoogleMap != null) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();

            Projection proj = mGoogleMap.getProjection();
            Point startPoint = proj.toScreenLocation(marker.getPosition());
            final LatLng startLatLng = proj.fromScreenLocation(startPoint);
            final long duration = 500;

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {

                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed
                            / duration);
                    double lng = t * newLocation.longitude + (1 - t)
                            * startLatLng.longitude;
                    double lat = t * newLocation.latitude + (1 - t)
                            * startLatLng.latitude;
                    marker.setPosition(new LatLng(lat, lng));

                    if (t < 1.0) {
                        handler.postDelayed(this, 16);
                    }
                }
            });
        }
    }

    private void removeOldMarks() {
        List<Driver> toRemove = new ArrayList<>();
        for (Driver oldDriver : mMarkers.keySet()) {
            boolean found = false;
            for (Driver driver : mDriverList) {
                if (driver.equals(oldDriver)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                toRemove.add(oldDriver);
            }
        }

        for (Driver oldDriver : toRemove) {
            mMarkers.remove(oldDriver).remove();
        }
    }

    private void executeTask() {
        if (mGoogleMap != null) {
            if (mTask != null) {
                mTask.cancel(true);
            }

            mTask = new LastLocationTask();
            mTask.setListener(this);
            mTask.execute(new LastLocationTask.Param(mGoogleMap.getProjection().getVisibleRegion().latLngBounds));
        }
    }

    private void forceExecute() {
        mHandler.removeCallbacks(mTaskHandler);
        mTaskHandler.run();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("drivers", mDriverList);
        outState.putBoolean("mapSetup", mMapSetup);
        if (mMyMarker != null){
            outState.putParcelable("markerLocation", mMyMarker.getPosition());
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_profile) {
            startActivity(new Intent(this, ProfileActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v.equals(mBtnRefresh)) {
            startRefresh();
        }

        if (v.equals(mBtnMyLocation)) {
            if (mGoogleMap != null && mLastLocation != null) {
                mUpdatingLocationMarker = true;

                LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, DEFAULT_ZOOM));
                if (mMyMarker != null) {
                    mMyMarker.setPosition(position);
                }
            }
        }

        if (v.equals(mBtnCallTaxi)) {
            showCard();
        }

        if (v.equals(mBtnRequestNegative)) {
            hideCard();
        }

        if (v.equals(mBtnRequestPositive)) {
            hideCard();
            startDriverRequest();
        }
    }

    private void startDriverRequest() {
        if (mGoogleMap != null) {
            mGoogleMap.snapshot(new GoogleMap.SnapshotReadyCallback() {
                @Override
                public void onSnapshotReady(Bitmap bitmap) {
                    Intent intent = new Intent(MapActivity.this, RequestActivity.class);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                    intent.putExtra(RequestActivity.ARG_MAP, stream.toByteArray());

                    try {
                        stream.close();
                    } catch (IOException e) {
                        Log.e("99Taxis", "IOException while closing stream");
                    }

                    intent.putExtra(RequestActivity.ARG_LOCATION, mMyMarker.getPosition());
                    startActivity(intent);
                }
            });
        }
    }

    private void hideCard() {
        if (mRunningAnim) return;

        mRunningAnim = true;
        int shortDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        ViewCompat.animate(mCardRequest).alpha(0f).scaleX(0f).scaleY(0f).setDuration(shortDuration)
                .setInterpolator(new FastOutLinearInInterpolator()).withEndAction(new Runnable() {
            @Override
            public void run() {
                mCardRequest.setVisibility(View.INVISIBLE);
            }
        }).start();


        ViewCompat.setAlpha(mBtnCallTaxi, 0f);
        mBtnCallTaxi.setVisibility(View.VISIBLE);
        ViewCompat.animate(mBtnCallTaxi).alpha(1f).setDuration(shortDuration).withEndAction(new Runnable() {
            @Override
            public void run() {
                ViewCompat.setAlpha(mBtnCallTaxi, 1f);
                mRunningAnim = false;
            }
        }).start();
    }

    private void showCard() {
        if (mRunningAnim) return;

        mRunningAnim = true;
        if (mGoogleMap != null && mMyMarker != null) {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMyMarker.getPosition(), DEFAULT_ZOOM));
        }

        mCardRequest.setPivotX(mBtnCallTaxi.getWidth() / 2);
        mCardRequest.setPivotY(mBtnCallTaxi.getHeight() / 2);

        ViewCompat.setScaleX(mCardRequest, 0f);
        ViewCompat.setScaleY(mCardRequest, 0f);
        ViewCompat.setAlpha(mCardRequest, 0f);
        mCardRequest.setVisibility(View.VISIBLE);

        int longDuration = getResources().getInteger(android.R.integer.config_longAnimTime);
        int shortDuration = getResources().getInteger(android.R.integer.config_shortAnimTime);

        ViewCompat.animate(mCardRequest).alpha(1f).scaleX(1f).scaleY(1f).setDuration(longDuration)
                .setInterpolator(new BounceInterpolator()).withEndAction(new Runnable() {
            @Override
            public void run() {
                ViewCompat.setScaleX(mCardRequest, 1f);
                ViewCompat.setScaleY(mCardRequest, 1f);
                ViewCompat.setAlpha(mCardRequest, 1f);
                mRunningAnim = false;
            }
        }).start();
        ViewCompat.animate(mBtnCallTaxi).alpha(0f).setDuration(shortDuration).withEndAction(new Runnable() {
            @Override
            public void run() {
                mBtnCallTaxi.setVisibility(View.GONE);
            }
        }).start();

    }

    private void startRefresh() {
        if (mRunningAnim) {
            return;
        }

        mRunningAnim = true;
        mBtnRefresh.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= 21) {
            mLoading.setVisibility(View.VISIBLE);
            Point start = new Point();
            start.y = (int) (mBtnRefresh.getY() + (mBtnRefresh.getHeight() / 2));
            start.x = (int) (mBtnRefresh.getX() + (mBtnRefresh.getWidth() / 2));

            float endRadius = (float) Math.sqrt(Math.pow(mLoading.getWidth(), 2) + Math.pow(mLoading.getHeight(), 2));
            Animator animator = ViewAnimationUtils.createCircularReveal(mLoading, start.x, start.y, 0f, endRadius);
            animator.setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    onEndStartRefresh();
                }
            });
            animator.start();
        } else {
            ViewCompat.setAlpha(mLoading, 0f);
            mLoading.setVisibility(View.VISIBLE);
            ViewCompat.animate(mLoading).alpha(1f).setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime)).withEndAction(new Runnable() {
                @Override
                public void run() {
                    onEndStartRefresh();
                }
            }).start();
        }

        forceExecute();
    }

    private void stopRefresh() {
        if (mRunningAnim) {
            return;
        }
        mRunningAnim = true;
        if (Build.VERSION.SDK_INT >= 21) {
            Point start = new Point();
            start.y = (int) (mBtnRefresh.getY() + (mBtnRefresh.getHeight() / 2));
            start.x = (int) (mBtnRefresh.getX() + (mBtnRefresh.getWidth() / 2));

            float startRadius = (float) Math.sqrt(Math.pow(mLoading.getWidth(), 2) + Math.pow(mLoading.getHeight(), 2));
            Animator animator = ViewAnimationUtils.createCircularReveal(mLoading, start.x, start.y, startRadius, 0f);
            animator.setDuration(getResources().getInteger(android.R.integer.config_longAnimTime));
            animator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mLoading.setVisibility(View.INVISIBLE);
                    onEndStopRefresh();
                }
            });
            animator.start();
        } else {
            ViewCompat.animate(mLoading).alpha(0f).setDuration(getResources().getInteger(android.R.integer.config_longAnimTime)).withEndAction(new Runnable() {
                @Override
                public void run() {
                    onEndStopRefresh();
                }
            }).start();
        }
    }

    private void onEndStopRefresh() {
        mBtnRefresh.setVisibility(View.VISIBLE);
        mRunningAnim = false;
        if (mTask != null && !mTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            startRefresh();
        }
    }

    private void onEndStartRefresh() {
        mRunningAnim = false;
        if (mTask != null && mTask.getStatus().equals(AsyncTask.Status.FINISHED)) {
            stopRefresh();
        }
    }

    @Override
    public void onTaskStart(Class<?> taskClass) {

    }

    @Override
    public void onTaskEnd(Class<?> taskClass, Object data, TaskException error) {
        if (taskClass.equals(LastLocationTask.class)) {
            if (error == null) {
                LastLocationTask.Result result = (LastLocationTask.Result) data;
                this.mDriverList = result.getDrivers();
                updateUI();

            }

            mHandler.postDelayed(mTaskHandler, mUpdateInterval);
            if (mLoading.isShown()) {
                stopRefresh();
            }
        }
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        mUpdatingLocationMarker = false;
    }

    @Override
    public void onMarkerDrag(Marker marker) {


    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        showCard();
    }

}
