package br.com.nntaxis.interview.view.spannable;


import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

import br.com.nntaxis.interview.R;
import br.com.nntaxis.interview.controller.App;

public class CustomSpan extends TypefaceSpan {

    private Typeface typeface;
    private int textSize;
    private int color = -1;

    private final Context context;

    public CustomSpan(Typeface typeface) {
        super("sans-serif");
        this.typeface = typeface;
        this.context = App.getInstance();
        this.textSize = R.dimen.text_body;
    }

    public CustomSpan(Typeface typeface, @ColorRes int color) {
        super("sans-serif");
        this.typeface = typeface;
        this.color = color;
        this.context = App.getInstance();
        this.textSize = R.dimen.text_body;
    }

    public CustomSpan(Typeface typeface, @ColorRes int color, @DimenRes int textSize) {
        super("sans-serif");
        this.typeface = typeface;
        this.color = color;
        this.context = App.getInstance();
        this.textSize = textSize;
    }

    @Override
    public void updateDrawState(@NonNull TextPaint ds) {
        applyCustomTypeFace(ds);
    }

    @Override
    public void updateMeasureState(@NonNull TextPaint paint) {
        applyCustomTypeFace(paint);
    }

    private void applyCustomTypeFace(Paint paint) {
        if (color != -1) {
            paint.setColor(ContextCompat.getColor(context, color));
        }

        paint.setTextSize(context.getResources().getDimension(textSize));
        paint.setTypeface(typeface);
    }


}



