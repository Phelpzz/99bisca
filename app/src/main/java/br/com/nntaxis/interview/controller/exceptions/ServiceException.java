package br.com.nntaxis.interview.controller.exceptions;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class ServiceException extends TaskException {

    private int code;
    private String message;

    public ServiceException() {
        super();
    }

    public ServiceException(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
