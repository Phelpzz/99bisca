package br.com.nntaxis.interview.controller.asynctasks;

import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import br.com.nntaxis.interview.controller.asynctasks.generic.AbstractTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.network.Network;
import br.com.nntaxis.interview.controller.utils.CommonUtils;
import br.com.nntaxis.interview.model.Driver;

/**
 * Created by FBisca on 02/10/2015.
 */
public class LastLocationTask extends AbstractTask<LastLocationTask.Param, Void, LastLocationTask.Result> {

    public static final String URL = "https://api.99taxis.com/lastLocations";

    @Override
    protected LastLocationTask.Result processInBackground(LastLocationTask.Param[] params) throws RuntimeException, JSONException {

        Network.Response response = Network.create(URL)
                .setMethod(Network.Method.GET)
                .addParameter("sw", CommonUtils.formatLatLng(params[0].mBounds.southwest))
                .addParameter("ne", CommonUtils.formatLatLng(params[0].mBounds.northeast))
                .request();

        if (response.getError() == null && response.getResponseCode() == 200) {
            return new Result(parseResponse(response.getResponse()));
        } else {
            throw new ServiceException();
        }
    }


    private ArrayList<Driver> parseResponse(String response) throws JSONException {
        ArrayList<Driver> drivers = new ArrayList<>();
        JSONArray driversArray = new JSONArray(response);
        for (int i = 0; i < driversArray.length(); i++) {
            Driver driver = new Driver(driversArray.getJSONObject(i));
            drivers.add(driver);
        }
        return drivers;
    }

    public static class Param {
        private LatLngBounds mBounds;

        public Param(LatLngBounds mBounds) {
            this.mBounds = mBounds;
        }
    }

    public class Result {
        private ArrayList<Driver> mDrivers;

        public Result(ArrayList<Driver> mDrivers) {
            this.mDrivers = mDrivers;
        }

        public ArrayList<Driver> getDrivers() {
            return mDrivers;
        }
    }

}
