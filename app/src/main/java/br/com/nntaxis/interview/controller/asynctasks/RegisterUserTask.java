package br.com.nntaxis.interview.controller.asynctasks;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.nntaxis.interview.controller.Constants;
import br.com.nntaxis.interview.controller.asynctasks.generic.AbstractTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.network.Network;
import br.com.nntaxis.interview.model.User;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class RegisterUserTask extends AbstractTask<RegisterUserTask.Param, Void, RegisterUserTask.Result> {

    @Override
    protected Result processInBackground(Param[] params) throws RuntimeException, JSONException {
        Network.Response response = Network.create(Constants.HOST + Constants.USER_SERVICE)
                .setMethod(Network.Method.POST)
                .addParameter("name", params[0].name)
                .request();

        if (response.getError() != null) {
            throw new TaskException(response.getError());
        }

        if (response.getResponseCode() == 201) {
            List<String> values = response.getHeaders().get("Location");

            if (values != null && !values.isEmpty()) {
                String url = values.get(0);
                response = Network.create(url).request();

                if (response.getError() != null) {
                    throw new TaskException(response.getError());
                }

                if (response.getResponseCode() == 200) {
                    JSONObject object = new JSONObject(response.getResponse());
                    User user = new User();
                    user.setId(object.getInt("id"));
                    user.setName(object.getString("name"));
                    user.setLocationUrl(url);
                    return new Result(user);
                }
            }

        }

        throw new ServiceException("", response.getResponseCode());
    }

    public static class Param {
        private String name;

        public Param(String name) {
            this.name = name;
        }
    }

    public class Result {
        private User user;

        public Result(User user) {
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }
}
