package br.com.nntaxis.interview.controller.asynctasks.generic;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import org.json.JSONException;

import br.com.nntaxis.interview.controller.App;
import br.com.nntaxis.interview.controller.exceptions.CancelledTaskException;
import br.com.nntaxis.interview.controller.exceptions.NotConnectedException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.interfaces.TaskListener;

/**
 * Created by FBisca on 12/11/2015.
 */
public abstract class AbstractTask<Param,Progress,Result> extends AsyncTask<Param, Progress, Result> {

    private TaskListener mListener;
    protected TaskException error;
    protected Status mStatus = Status.PENDING;

    public AbstractTask() {
        super();
    }

    public AbstractTask(TaskListener listener) {
        super();
        this.mListener = listener;
    }

    @Override
    protected final Result doInBackground(Param[] params) {
        try {
            if(error == null) {
                return processInBackground(params);
            }
        } catch (TaskException e) {
            error = e;
        } catch (Exception e) {
            error = new TaskException(e);
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mStatus = Status.RUNNING;
        if (mListener != null) {
            mListener.onTaskStart(getClass());
        }

        if (!isOnline()) {
            error = new NotConnectedException();
        }
    }


    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        mStatus = Status.FINISHED;
        if (mListener != null) {
            if (isCancelled()) {
                error = new CancelledTaskException();
            }

            mListener.onTaskEnd(getClass(), result, error);
        }

        close();
    }

    public void close() {
        mListener = null;
    }

    @Override
    protected void onCancelled(Result result) {
        super.onCancelled(result);
        mStatus = Status.FINISHED;
        if (mListener != null) {
            mListener.onTaskEnd(getClass(), result, new CancelledTaskException());
        }

        close();
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)
                App.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public TaskListener getListener() {
        return mListener;
    }

    public void setListener(TaskListener mListener) {
        this.mListener = mListener;
    }

    public TaskException getError() {
        return error;
    }

    public Status getTaskStatus() {
        return mStatus;
    }

    protected abstract Result processInBackground(Param[] params) throws RuntimeException, JSONException;

}
