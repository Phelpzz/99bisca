package br.com.nntaxis.interview.controller.asynctasks;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.nntaxis.interview.controller.Constants;
import br.com.nntaxis.interview.controller.asynctasks.generic.AbstractTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.network.Network;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class RequestDriverTask extends AbstractTask<Void, Void, String> {

    @Override
    protected String processInBackground(Void[] voids) throws RuntimeException, JSONException {
        Network.Response response = Network.create(Constants.HOST + Constants.RIDE_SERVICE)
                .setMethod(Network.Method.PUT)
                .request();

        if (response.getError() != null) {
            throw new TaskException(response.getError());
        }

        if (response.getResponseCode() == 201) {
            List<String> values = response.getHeaders().get("Location");

            if (values != null && !values.isEmpty()) {
                String url = values.get(0);
                response = Network.create(url).request();

                if (response.getError() != null) {
                    throw new TaskException(response.getError());
                }

                JSONObject object = new JSONObject(response.getResponse());
                return object.getString("msg");
            } else {
                throw new TaskException();
            }

        } else if (response.getResponseCode() == 410) {
            JSONObject object = new JSONObject(response.getResponse());
            throw new ServiceException(object.getString("msg"), 410);
        } else {
            throw new ServiceException();
        }

    }


}
