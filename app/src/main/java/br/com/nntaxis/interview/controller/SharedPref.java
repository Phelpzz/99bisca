package br.com.nntaxis.interview.controller;

import android.content.Context;
import android.content.SharedPreferences;

import br.com.nntaxis.interview.model.User;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class SharedPref {

    private static final String SHARED_PREF_NAME = "99prefs";

    public static SharedPreferences get() {
        return App.getInstance().getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static void saveUser(User user) {
        SharedPreferences.Editor editor = get().edit();
        editor.putInt("user_id", user.getId());
        editor.putString("user_name", user.getName());
        editor.apply();
    }

    public static User getUser() {
        SharedPreferences sp = get();
        int id = sp.getInt("user_id", -1);
        if (id == -1) {
            return null;
        }

        User user = new User();
        user.setId(id);
        user.setName(sp.getString("user_name", ""));
        return user;
    }

    public static void clear() {
        get().edit().clear().apply();
    }


}
