package br.com.nntaxis.interview.view;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import br.com.nntaxis.interview.R;
import br.com.nntaxis.interview.controller.SharedPref;
import br.com.nntaxis.interview.controller.asynctasks.EditUserTask;
import br.com.nntaxis.interview.controller.asynctasks.RegisterUserTask;
import br.com.nntaxis.interview.controller.exceptions.ServiceException;
import br.com.nntaxis.interview.controller.exceptions.TaskException;
import br.com.nntaxis.interview.controller.interfaces.TaskListener;
import br.com.nntaxis.interview.model.User;
import br.com.nntaxis.interview.view.generic.GenericActivity;

/**
 * Created by felip_000 on 12/11/2015.
 */
public class ProfileActivity extends GenericActivity implements TextView.OnEditorActionListener, View.OnClickListener, TaskListener {

    private TextView mTxtTitle;
    private TextInputLayout mInpLayoutName;
    private EditText mInpName;
    private Button mBtnSave, mBtnSignOut;
    private View mLoading;
    private ScrollView mScrollView;
    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUser = SharedPref.getUser();

        setContentView(R.layout.layout_profile);
        loadViews();
    }

    private void loadViews() {
        mTxtTitle = (TextView) findViewById(R.id.txt_title);
        mBtnSave = (Button) findViewById(R.id.btn_save);
        mBtnSignOut = (Button) findViewById(R.id.btn_signout);
        mInpName = (EditText) findViewById(R.id.inp_name);
        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mInpLayoutName = (TextInputLayout) findViewById(R.id.inp_layout_name);
        mLoading = findViewById(R.id.progress);

        mInpName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mInpLayoutName.isErrorEnabled()) {
                    mInpLayoutName.setErrorEnabled(false);
                    mInpLayoutName.setError(null);
                    Drawable drawable = DrawableCompat.wrap(mInpName.getBackground());
                    DrawableCompat.setTint(drawable, ContextCompat.getColor(ProfileActivity.this, R.color.colorAccent));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mInpName.setOnEditorActionListener(this);
        mBtnSave.setOnClickListener(this);
        mBtnSignOut.setOnClickListener(this);

        updateUser();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void updateUser() {
        if (mUser != null) {
            mBtnSave.setText(R.string.lbl_update);
            mBtnSignOut.setVisibility(View.VISIBLE);
            mInpName.setText(mUser.getName());
            mInpName.setSelection(mInpName.getText().length());
            mTxtTitle.setText(getString(R.string.lbl_profile, mUser.getName()));
        } else {
            mBtnSave.setText(R.string.lbl_signin);
            mInpName.setText("");
            mBtnSignOut.setVisibility(View.GONE);
            mTxtTitle.setText(R.string.lbl_profile_not_logged);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            executeTask();
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        if (v.equals(mBtnSave)) {
            executeTask();
        }

        if (v.equals(mBtnSignOut)) {
            SharedPref.clear();

            mUser = null;
            updateUser();
        }
    }

    private void executeTask() {
        String name = mInpName.getText().toString();
        if (name.trim().isEmpty()) {
            mInpLayoutName.setErrorEnabled(true);
            mInpLayoutName.setError(getString(R.string.lbl_name_not_empty));
            return;
        }

        if (mUser == null) {
            RegisterUserTask task = new RegisterUserTask();
            task.setListener(this);
            task.execute(new RegisterUserTask.Param(name));
        } else {
            mUser.setName(name);

            EditUserTask task = new EditUserTask();
            task.setListener(this);
            task.execute(new EditUserTask.Param(mUser));
        }

    }

    @Override
    public void onTaskStart(Class<?> taskClass) {
        mLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskEnd(Class<?> taskClass, Object data, TaskException error) {
        mLoading.setVisibility(View.GONE);

        if (taskClass.equals(RegisterUserTask.class)) {
            if (error == null) {
                RegisterUserTask.Result result = (RegisterUserTask.Result) data;

                SharedPref.saveUser(result.getUser());
                mUser = result.getUser();
                updateUser();

                Snackbar.make(mScrollView, "Nome salvo com sucesso", Snackbar.LENGTH_SHORT).show();
            } else if (error instanceof ServiceException){
                ServiceException e = (ServiceException) error;
                if (e.getCode() == 400) {
                    mInpLayoutName.setErrorEnabled(true);
                    mInpName.setError("Nome inválido");
                }
            } else {
                Snackbar.make(mScrollView, "Houve um erro de comunicação", Snackbar.LENGTH_SHORT).show();
            }
        }

        if (taskClass.equals(EditUserTask.class)) {
            if (error == null) {
                EditUserTask.Result result = (EditUserTask.Result) data;

                SharedPref.saveUser(result.getUser());
                mUser = result.getUser();
                updateUser();

                Snackbar.make(mScrollView, "Nome atualizado com sucesso", Snackbar.LENGTH_SHORT).show();
            } else if (error instanceof ServiceException){
                ServiceException e = (ServiceException) error;
                if (e.getCode() == 400) {
                    mInpLayoutName.setErrorEnabled(true);
                    mInpName.setError("Nome inválido");
                } else {
                    Snackbar.make(mScrollView, "Erro ao atualizar seu nome.", Snackbar.LENGTH_SHORT).show();
                }
            } else {
                Snackbar.make(mScrollView, "Houve um erro de comunicação", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
